﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TICA_ODA_WebReport
{
    public partial class RPT_Finance_country : System.Web.UI.Page
    {
        double result = 0.00;
        double n1 = 0.00;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Class1.BindDDlYear(ddlBubgetYear);
                BindData();
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindData();

            if (txtDateStart.Text != "" && txtDateTo.Text != "")
            {
                lblHeadTable.Text = "  วันที่ " + txtDateStart.Text + " - " + txtDateTo.Text + " ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            if (txtDateStart.Text == "")
            {
                lblHeadTable.Text = "  วันที่ " + txtDateTo.Text + " ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            if (txtDateTo.Text == "")
            {
                lblHeadTable.Text = "  วันที่ " + txtDateStart.Text + " ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            if (txtDateStart.Text == "" && txtDateTo.Text == "")
            {
                lblHeadTable.Text = "  ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            if (ddlBubgetYear.SelectedIndex ==0)
            {
                lblHeadTable.Text = " ทุกปีงบประมาณ";
            }


        }

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            Label lblCountry = (Label)e.Item.FindControl("lblCountry");
            Label lblList = (Label)e.Item.FindControl("lblList");
            Label lblDateStart = (Label)e.Item.FindControl("lblDateStart");
            Label lblDateTo = (Label)e.Item.FindControl("lblDateTo");
            Label lblAllocate = (Label)e.Item.FindControl("lblAllocate");
            Label lblLoan = (Label)e.Item.FindControl("lblLoan");
            Label lblDisburse = (Label)e.Item.FindControl("lblDisburse");
            Label lblBalance = (Label)e.Item.FindControl("lblBalance");

            DataRowView drv = (DataRowView)e.Item.DataItem;


            lblCountry.Text = drv["country_name"].ToString();
            lblList.Text = drv["project_name"].ToString();
            if (drv["project_start"].ToString() != "")
            {
                //lblDateStart.Text = Class1.DateTimeToThaiDateTime((DateTime)drv["project_start"]);
                //yyyy/mm/dd
                string[] mlDate = drv["project_start"].ToString().Split('-');
                if (mlDate.Length == 3)
                {
                    lblDateStart.Text = mlDate[2] + "/" + mlDate[1] + "/" + mlDate[0];
                }
            }
            else
            {
                lblDateStart.Text = "";
            }
            if (drv["project_end"].ToString() != "")
            {
                //yyyy/mm/dd
                //lblDateTo.Text = Class1.DateTimeToThaiDateTime((DateTime)drv["project_end"]);
                    string[] project_end = drv["project_end"].ToString().Split('-');
                    if (project_end.Length == 3)
                    {
                        lblDateTo.Text = project_end[2] + "/" + project_end[1] + "/" + project_end[0];
                    }
                }
            else
            {
                lblDateTo.Text = "";
            }

            if (drv["dis_budget"].ToString() != "")
            {

                lblAllocate.Text = Class1.StringFormatNumber(drv["dis_budget"].ToString(), 2);
            }
            else
            {
                lblAllocate.Text = "0.00";
            }
            if (drv["ml_amount"].ToString() != "")
            {

                lblLoan.Text = Class1.StringFormatNumber(drv["ml_amount"].ToString(), 2);
            }
            else
            {
                lblLoan.Text = "0.00";
            }
            if (drv["mp_amount"].ToString() !="")
            {
                lblDisburse.Text = drv["mp_amount"].ToString();
            }
            else
            {
                lblDisburse.Text = "0.00";
            }
            
            if (drv["mp_amount"].ToString() != "")
            {

                lblDisburse.Text = Class1.StringFormatNumber(drv["mp_amount"].ToString(), 2);
            }
            else
            {
                lblLoan.Text = "0.00";
            }
            double dis_budget = Class1.CDBL(Class1.StringFormatNumber(drv["dis_budget"].ToString(), 2));
            double ml_amount = Class1.CDBL(Class1.StringFormatNumber(drv["ml_amount"].ToString(), 2));
            double mp_amount = Class1.CDBL(Class1.StringFormatNumber(drv["mp_amount"].ToString(), 2));
            double result = dis_budget - (ml_amount + mp_amount);
           
                lblBalance.Text = Class1.StringFormatNumber(result.ToString(), 2);
           
          
           

            n1 += result;



            string strSumbalance = n1.ToString();
            lblSumBalance.Text = Class1.StringFormatNumber(strSumbalance.ToString(), 2);
            //Session["RPT_Finance_project_lblBalance"] = lblBalance.Text;

        }
        private void BindData()
        {


            //string SQL = "SELECT project.project_id, project.project_name, country.country_name, project.project_start," + "\n"; 
            // SQL += " project.project_end, SUM( Budget.dis_budget ) AS dis_budget, SUM( money_loan.ml_amount ) AS ml_amount, SUM( project_paid.mp_amount ) AS mp_amount, money_loan.ml_year, project_paid.mp_year" + "\n";
            //SQL += " FROM project" + "\n";
            //SQL += " INNER JOIN activity ON project.project_id = activity.project_id" + "\n";
            //SQL += " INNER JOIN aid ON activity.activity_id = aid.activity_id" + "\n";
            //SQL += " INNER JOIN recipient ON aid.aid_id = recipient.aid_id" + "\n";
            //SQL += " LEFT JOIN country ON recipient.country_id = country.country_id" + "\n";
            //SQL += " LEFT JOIN Budget ON project.project_id = Budget.project_id" + "\n";
            //SQL += " LEFT JOIN money_loan ON aid.aid_id = money_loan.aid_id" + "\n";
            //SQL += " LEFT JOIN project_paid ON project.project_id = project_paid.project_id";

        string BudgetYear = "";
            string Filter = "";
            if (txtSearch.Text != "")
            {
                Filter += " (country_name like '%" + txtSearch.Text + "%' OR project_name like '%" + txtSearch.Text + "%')  AND" + "\n";
            }
            if (txtDateStart.Text != "" && txtDateTo.Text != "")
            {
                string StartDate = (Class1.CINT(Convert.ToDateTime(txtDateStart.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString() + Convert.ToDateTime(txtDateStart.Text).ToString("-MM-dd", new System.Globalization.CultureInfo("en-US"));
                string EndDate = (Class1.CINT(Convert.ToDateTime(txtDateTo.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString() + Convert.ToDateTime(txtDateTo.Text).ToString("-MM-dd", new System.Globalization.CultureInfo("en-US"));
                Filter += " project_start>='" + StartDate + "' AND project_start<='" + EndDate + "'  AND" + "\n";
            }
            else
            {
                if (txtDateStart.Text !="")
                {
                    string StartDate = (Class1.CINT(Convert.ToDateTime(txtDateStart.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString() + Convert.ToDateTime(txtDateStart.Text).ToString("-MM-dd", new System.Globalization.CultureInfo("en-US"));
                    Filter += " project_start>='" + StartDate + "'  AND" + "\n";
                }
                if (txtDateTo.Text !="")
                {
                    string EndDate = (Class1.CINT(Convert.ToDateTime(txtDateTo.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString() + Convert.ToDateTime(txtDateTo.Text).ToString("-MM-dd", new System.Globalization.CultureInfo("en-US"));
                    Filter += " project_start<='" + EndDate + "'  AND" + "\n";
                }
            }

            if (ddlBubgetYear.Text != "")
            {
                if (ddlBubgetYear.SelectedIndex == 0)
                {
                    Filter += "  AND";
                    BudgetYear = "รายงานการใช้จ่ายเงินอุดหนุนทุกปีงบประมาณ";
                }
                else
                {
                    Filter += " ml_year='" + ddlBubgetYear.SelectedValue + "'" + " OR mp_year='" + ddlBubgetYear.SelectedValue + "'" + "  AND" + "\n";
                    BudgetYear = "รายงานการใช้จ่ายเงินอุดหนุนปีงบประมาณ " + ddlBubgetYear.Text + "";
                }

            }
            if (!string.IsNullOrEmpty(Filter)) /* + Filter.Substring(0, Filter.Length - 4)+ */
            {
                Filter = "  " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }

            DataTable DT = new DataTable();
            DataSet DS = new DataSet();

            //DT = Class1.ExecuteTable(SQL);
            //DT.TableName = "DataTable";
            //DS.Tables.Add(DT);
            //DS.DataSetName = "Dataset";
            //DS.WriteXml("E:\\Test\\qry_country.xml", XmlWriteMode.WriteSchema);
            //string random = DateTime.Now.ToOADate().ToString();



            

            string tmpXML = "C:\\webroot\\oda\\TempXML\\";
            if (System.IO.Directory.Exists(tmpXML) == false)
                System.IO.Directory.CreateDirectory(tmpXML);

            string FileName = tmpXML + "qry_country.xml";

            if (System.IO.File.Exists(FileName) == true)
            {
                System.IO.File.SetAttributes(FileName, System.IO.FileAttributes.Normal);
                System.IO.File.Delete(FileName);
            }


            string strXML = ODA_WebService.GetDataFromURL("http://oda.mfa.go.th/query/query_country.php", "", ODA_WebService.ContentType.Text, ODA_WebService.FormatReturnData.XML);
            if (strXML.Trim() != "")
            {
                strXML = ODA_WebService.StringReplace(strXML,"&","&amp;");

                System.IO.File.WriteAllText(FileName, strXML);
            }


            if (System.IO.File.Exists(FileName) == true)
            {
                DS.ReadXml(FileName);
                DT = DS.Tables[0];
                DT.DefaultView.RowFilter = Filter;
                double sumAllocate = 0.00;
                double sumLaon = 0.00;
                double sumPaid = 0.00;
                double sumBalance = 0.00;
                if (DT.DefaultView.Count > 0)
                {

                    for (int i = 0; i < DT.DefaultView.Count; i++)
                    {
                        if (DT.DefaultView[i]["dis_budget"].ToString() != "")
                        {

                            sumAllocate = sumAllocate + Class1.CDBL(Class1.StringFormatNumber(DT.DefaultView[i]["dis_budget"].ToString(), 2));
                        }

                    }
                    if (sumAllocate > 0)
                    {
                        lblAllocate.Text = Class1.StringFormatNumber(sumAllocate, 2);
                    }
                    else
                    {
                        lblAllocate.Text = "0.00";
                    }
                }
                else
                {
                    lblAllocate.Text = "0.00";
                }

                if (DT.DefaultView.Count > 0)
                {

                    for (int i = 0; i < DT.DefaultView.Count; i++)
                    {
                        if (DT.DefaultView[i]["ml_amount"].ToString() != "")
                        {

                            sumLaon = sumLaon + Class1.CDBL(Class1.StringFormatNumber(DT.DefaultView[i]["ml_amount"].ToString(), 2));
                        }

                    }
                    if (sumLaon > 0)
                    {
                        lblLoan.Text = Class1.StringFormatNumber(sumLaon, 2);
                    }
                    else
                    {
                        lblLoan.Text = "0.00";
                    }
                }
                else
                {
                    lblLoan.Text = "0.00";
                }


                if (DT.DefaultView.Count > 0)
                {

                    for (int i = 0; i < DT.DefaultView.Count; i++)
                    {
                        if (DT.DefaultView[i]["mp_amount"].ToString() != "")
                        {

                            sumPaid = sumPaid + Class1.CDBL(Class1.StringFormatNumber(DT.DefaultView[i]["mp_amount"].ToString(), 2));
                        }

                    }
                    if (sumPaid > 0)
                    {
                        lblPaid.Text = Class1.StringFormatNumber(sumPaid, 2);
                    }
                    else
                    {
                        lblPaid.Text = "0.00";
                    }
                }
                else
                {
                    lblPaid.Text = "0.00";
                }

                if (DT.DefaultView.Count > 0)
                {
                    lblShowCountData.Text = "พบ " + Class1.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
                }
                else
                {
                    lblShowCountData.Text = "ไม่พบรายการ";
                }
                rptList.DataSource = DT.DefaultView.ToTable(); ;
                rptList.DataBind();

                Session["RPT_Finance_country"] = DT.DefaultView.ToTable(); 
                Session["RPT_Finance_country_BudgetYear"] = BudgetYear;

                Session["RPT_Finance_project_lblAllocate"] = lblAllocate.Text;
                Session["RPT_Finance_project_lblLoan"] = lblLoan.Text;
                Session["RPT_Finance_project_lblPaid"] = lblPaid.Text;
                Session["RPT_Finance_project_lblSumBalance"] = lblSumBalance.Text;
                
            }
        }
           
    }
}