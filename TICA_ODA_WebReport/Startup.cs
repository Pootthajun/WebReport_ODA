﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TICA_ODA_WebReport.Startup))]
namespace TICA_ODA_WebReport
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
