﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TICA_ODA_WebReport
{
    public partial class Print_Finance_country : System.Web.UI.Page
    {
        ReportDocument cc;
        protected void Page_Load(object sender, EventArgs e)
        {
            cc = new ReportDocument();
            cc.Load(Server.MapPath("Report/RPT_Finance_country.rpt"));
            //cc.Subreports["C_BHV"].SetDataSource(BT);
            DataTable DT = (DataTable)Session["RPT_Finance_country"];
            DT.Columns.Add("BudgetBalance");
            for (int i = 0; DT.Rows.Count > i; i++)
            {
                if (DT.Rows[i]["dis_budget"].ToString() == "")
                {
                    DT.Rows[i]["dis_budget"] = "0.00";
                }
                if (DT.Rows[i]["ml_amount"].ToString() == "")
                {
                    DT.Rows[i]["ml_amount"] = "0.00";
                }
                if (DT.Rows[i]["mp_amount"].ToString() == "")
                {
                    DT.Rows[i]["mp_amount"] = "0.00";
                }
                double project_budget = Class1.CDBL(Class1.StringFormatNumber(DT.Rows[i]["dis_budget"].ToString(), 2));
                double dis_budget = Class1.CDBL(Class1.StringFormatNumber(DT.Rows[i]["dis_budget"].ToString(), 2));
                double mp_amount = Class1.CDBL(Class1.StringFormatNumber(DT.Rows[i]["mp_amount"].ToString(), 2));
                double result = project_budget - (dis_budget + mp_amount);
                DT.Rows[i]["BudgetBalance"] = Class1.StringFormatNumber(result.ToString(), 2);
            }
            cc.SetDataSource(DT);
            CrystalReportViewer1.ReportSource = cc;
            DT.Columns.Remove("BudgetBalance");
            cc.SetParameterValue("BudgetYear", Session["RPT_Finance_country_BudgetYear"].ToString());

            cc.SetParameterValue("sumDisbudget", Session["RPT_Finance_project_lblAllocate"].ToString());
            cc.SetParameterValue("sumMlAmount", Session["RPT_Finance_project_lblLoan"].ToString());
            cc.SetParameterValue("sumMpAmount", Session["RPT_Finance_project_lblPaid"].ToString());
           // cc.SetParameterValue("BudgetBalance", Session["RPT_Finance_project_lblBalance"].ToString());
            cc.SetParameterValue("sumBBalance", Session["RPT_Finance_project_lblSumBalance"].ToString());

            byte[] B;
            switch (Request.QueryString["Mode"].ToUpper())
            {
                case "PDF":
                    Response.AddHeader("Content-Type", "application/pdf");
                    Response.AppendHeader("Content-Disposition", "filename=รายงานการใช้จ่ายเงินอุดหนุน_" + DateTime.Now.Year + DateTime.Now.Month.ToString().PadLeft(2, Class1.chr0) + DateTime.Now.Day.ToString().PadLeft(2, Class1.chr0) + ".pdf");
                    B = Class1.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
                    Response.BinaryWrite(B);
                    break;
                case "EXCEL":
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AppendHeader("Content-Disposition", "filename=รายงานการใช้จ่ายเงินอุดหนุน_" + DateTime.Now.Year + DateTime.Now.Month.ToString().PadLeft(2, Class1.chr0) + DateTime.Now.Day.ToString().PadLeft(2, Class1.chr0) + ".xls");
                    B = Class1.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
                    Response.BinaryWrite(B);
                    break;
                default:
                    break;

            }

        }
    }
}