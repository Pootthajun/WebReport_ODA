﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Text;
using System.Net.Mime;

namespace TICA_ODA_WebReport
{
    public class Class1
    {
        public static string GetConnectionString()
        {
            string ret = "";

            try
            {
                //DatabaseConfigData db = OPMSqlDb.getDatebaseConfig("MySqlDPIS");

                ret = "Server=localhost;Database=oda_db;Uid=root;Pwd=;";
               // ret = "Server=192.168.1.28;Port=3306;Database=oda_db;Uid=root;Pwd=;";

            }
            catch (Exception ex)
            {
                ret = "";
            }
            return ret;
        }

        private static MySqlConnection GetConnection()
        {
            string connStr = GetConnectionString();
            MySqlConnection conn = null;
            try
            {

                //conn = MySqlConnectDB.GetConnection(connStr);
                conn = new MySqlConnection(connStr);
            }
            catch (Exception ex)
            {
                conn = null;
            }
            return conn;
        }


        public static DataTable ExecuteTable(string sql)
        {
            MySqlCommand cmd = new MySqlCommand();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            adapter.SelectCommand = cmd;
            DataTable dt = new DataTable();

            try
            {
                MySqlConnection conn = GetConnection();

                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;

                adapter.Fill(dt);
                adapter.Dispose();

                cmd.Dispose();
                conn.Close();
            }
            catch (Exception ex)
            {
                dt = new DataTable();
            }
            return dt;
        }

        public static string ExecuteNonQuery(string sql)
        {
            string ret = "false";
            try
            {
                MySqlConnection conn = GetConnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;
                cmd.ExecuteNonQuery();

                conn.Close();
                cmd.Dispose();

                ret = "true";
            }
            catch (Exception ex)
            {
                ret = "false|Exception : " + ex.Message + Environment.NewLine + ex.StackTrace;
            }
            return ret;
        }

        public static string ExecuteNonQuery(string sql, MySqlParameter[] param)
        {
            string ret = "false";
            try
            {
                MySqlConnection conn = GetConnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;

                if (param != null)
                {
                    foreach (MySqlParameter p in param)
                    {
                        if (p != null)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }
                }

                cmd.ExecuteNonQuery();
                ret = "true";
            }
            catch (Exception ex)
            {
                ret = "false|Exception " + ex.Message + "\n" + ex.StackTrace.ToString();
            }

            return ret;
        }

        public static DataTable ExecuteTable(string sql, MySqlTransaction trans)
        {
            return ExecuteTable(sql, trans);
        }

        public static DataTable ExecuteTable(string sql, MySqlParameter[] param)
        {
            MySqlCommand cmd = new MySqlCommand();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            adapter.SelectCommand = cmd;
            DataTable dt = new DataTable();

            try
            {
                MySqlConnection conn = GetConnection();

                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;

                if (param != null)
                {
                    foreach (MySqlParameter p in param)
                    {
                        if (p != null)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }
                }

                adapter.Fill(dt);
                adapter.Dispose();

                cmd.Dispose();
                conn.Close();
            }
            catch (Exception ex)
            {
                dt = new DataTable();
            }
            return dt;
        }

        public static bool ExecuteNonQuery(string sql, MySqlTransaction trans)
        {
            return ExecuteNonQuery(sql, trans);
        }

        private static MySqlParameter setParameter(string pName, MySqlDbType pType, object pValue)
        {
            MySqlParameter p = new MySqlParameter(pName, pType);
            p.Value = pValue;

            return p;
        }

        public static MySqlParameter setText(string pName, string pValue)
        {
            return setParameter(pName, MySqlDbType.Text, pValue);
        }
        public static MySqlParameter setDouble(string pName, object pValue)
        {
            return setParameter(pName, MySqlDbType.Double, pValue);
        }

        public static MySqlParameter setBigint(string pName, object pValue)
        {
            return setParameter(pName, MySqlDbType.Int64, pValue);
        }
        public static MySqlParameter setInt(string pName, object pValue)
        {
            return setParameter(pName, MySqlDbType.Int16, pValue);
        }
        public static MySqlParameter setDateTime(string pName, DateTime pValue)
        {
            return setParameter(pName, MySqlDbType.DateTime, pValue);
        }


        public static bool CheckConnection()
        {
            bool ret = false;
            try
            {
                MySqlConnection conn = GetConnection();
                if (conn.State == ConnectionState.Open)
                    ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }

        public static string StringFormatNumber(string Str, int DecimalPlace)
        {
            try
            {
                double dbl = Convert.ToDouble(Str);
                string Format = "{0:#,0";
                if (DecimalPlace > 0)
                {
                    Format += ".";
                }
                for (int i = 1; i <= DecimalPlace; i++)
                {
                    Format += "0";
                }
                Format += "}";
                return string.Format(Format, dbl);
            }
            catch (Exception ex)
            {

                return Str = "0.00";
            }
           
        }

        public static string StringFormatNumber(string Str)
        {
            return StringFormatNumber(Str, 2);
        }

        public static string StringFormatNumber(object Obj, int DecimalPlace)
        {
            return StringFormatNumber(Obj.ToString(), DecimalPlace);
        }

        public static string StringFormatNumber(object Obj)
        {
            return StringFormatNumber(Obj, 2);
        }

        public static double CDBL(string str)
        {
            return Convert.ToDouble(str);
        }

        public static double CDBL(int input)
        {
            return Convert.ToDouble(input);
        }

        public static double CDBL(object Obj)
        {
            return Convert.ToDouble(Obj);
        }

        //ทศนิยม 2 ตำแหน่ง
        public static string StringFormatNumber(Double val)
        {
            return String.Format("{0:F2}", val);
        }

        public static int CINT(string str)
        {
            try
            {
                return Convert.ToInt32(str);
            }
            catch
            {
                return 0;
            }

        }

        public static int CINT(double input)
        {
            return Convert.ToInt32(input);
        }

        public static int CINT(object Obj)
        {
            return Convert.ToInt32(Obj);
        }


        public static bool CBOOL(string str)
        {
            return Convert.ToBoolean(str);
        }

        public static bool CBOOL(object str)
        {
            if (str != null)
            {
                return Convert.ToBoolean(str.ToString());
            }
            else
            {
                return false;
            }
        }


        //ทศนิยม ปัดขึ้น .5  0
        public static string StringFormatNumber_1P(Double val)
        {
            return String.Format("{0:00.0}", val);
        }

        public static char chr0 = '0';

        public static char CCHR(string str)
        {
            if (str != "")
            {
                return Convert.ToChar(str.Substring(0, 1));
            }
            else {
                return Convert.ToChar("");
            }
        }

        public string ReportProgrammingDate(DateTime Input)
        {
            return Input.Year + "-" + Input.Month.ToString().PadLeft(2, chr0) + "-" + Input.Day.ToString().PadLeft(2, chr0);
        }

        public string ReportProgrammingDotDate(DateTime Input)
        {
            return Input.Day.ToString().PadLeft(2, chr0) + "." + Input.Month.ToString().PadLeft(2, chr0) + "." + Input.Year;
        }

        public string ReportProgrammingFullDate(DateTime Input)
        {
            string StrDate = Input.Month.ToString();
            StrDate = ReportShortMonthEnglish(Input.Month);
            return Input.Day.ToString().PadLeft(2, chr0) + "-" + StrDate + "-" + Input.Year;
        }

        public bool IsProgrammingDate(string Input)
        {
            try
            {
                System.DateTime Temp = DateTime.Parse(Input);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool IsProgrammingDate(string Input, string Format)
        {
            try
            {
                CultureInfo Provider = CultureInfo.GetCultureInfo("en-US");
                DateTime Temp = DateTime.ParseExact(Input, Format, Provider);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public bool IsValidEmailFormat(string input)
        {
            return Regex.IsMatch(input, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        }

        public string OriginalFileName(string FullPath)
        {
            return FullPath.Substring(FullPath.LastIndexOf("\\") + 1);
        }

        public string ReportMonthThai(int MonthID)
        {
            switch (MonthID)
            {
                case 1:
                    return "มกราคม";
                case 2:
                    return "กุมภาพันธ์";
                case 3:
                    return "มีนาคม";
                case 4:
                    return "เมษายน";
                case 5:
                    return "พฤษภาคม";
                case 6:
                    return "มิถุนายน";
                case 7:
                    return "กรกฎาคม";
                case 8:
                    return "สิงหาคม";
                case 9:
                    return "กันยายน";
                case 10:
                    return "ตุลาคม";
                case 11:
                    return "พฤศจิกายน";
                case 12:
                    return "ธันวาคม";
                default:
                    return "";
            }
        }

        public string ReportMonthThaiShort(int MonthID)
        {
            switch (MonthID)
            {
                case 1:
                    return "ม.ค.";
                case 2:
                    return "ก.พ.";
                case 3:
                    return "มี.ค.";
                case 4:
                    return "เม.ย.";
                case 5:
                    return "พ.ค.";
                case 6:
                    return "มิ.ย.";
                case 7:
                    return "ก.ค.";
                case 8:
                    return "ส.ค.";
                case 9:
                    return "ก.ย.";
                case 10:
                    return "ต.ค.";
                case 11:
                    return "พ.ย.";
                case 12:
                    return "ธ.ค.";
                default:
                    return "";
            }
        }

        public string ReportMonthEnglish(int MonthID)
        {
            switch (MonthID)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return "";
            }
        }

        public string ReportShortMonthEnglish(int MonthID)
        {
            switch (MonthID)
            {
                case 1:
                    return "Jan";
                case 2:
                    return "Feb";
                case 3:
                    return "Mar";
                case 4:
                    return "Apr";
                case 5:
                    return "May";
                case 6:
                    return "Jun";
                case 7:
                    return "Jul";
                case 8:
                    return "Aug";
                case 9:
                    return "Sep";
                case 10:
                    return "Oct";
                case 11:
                    return "Nov";
                case 12:
                    return "Dec";
                default:
                    return "";
            }
        }


        public int ReportMonthEnglishToNumber(string DateFullName)
        {
            switch (DateFullName)
            {
                case "January":
                    return 1;
                case "February":
                    return 2;
                case "March":
                    return 3;
                case "April":
                    return 4;
                case "May":
                    return 5;
                case "June":
                    return 6;
                case "July":
                    return 7;
                case "August":
                    return 8;
                case "September":
                    return 9;
                case "October":
                    return 10;
                case "November":
                    return 11;
                case "December":
                    return 12;
                default:
                    return 0;
            }
        }

        public string ReportThaiDate(DateTime TheDate)
        {
            string Result = TheDate.Day + "/" + ReportMonthThaiShort(TheDate.Month) + "/" + (TheDate.Year + 543);
            return Result;
        }
        public string ReportThaiDateTime(DateTime TheDate)
        {
            string Result = ReportThaiDate(TheDate) + " " + TheDate.Hour.ToString().PadLeft(2, chr0) + ":" + TheDate.Minute.ToString().PadLeft(2, chr0);
            return Result;
        }
        public static string DateTimeToThaiDateTime(DateTime TheDate)
        {
            return TheDate.Day.ToString().PadLeft(2, chr0) + "/" + TheDate.Month.ToString().PadLeft(2, chr0) + "/" + (TheDate.Year);
        }
        public static void BindDDlYear(DropDownList ddl)
        {
            //DataTable DT = GetAllAssessmentRound();
            //string[] Col = { "R_Year" };
            //DT = DT.DefaultView.ToTable(true, Col);
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทุกปีงบประมาณ", "0"));
            //------------2557 = ปีเริ่มต้น +544 = ปีปัจจุบัน + 1 
            for (int i = 2550; i <= DateTime.Now.Year + 543; i++)
            {
                ListItem Item = new ListItem(i.ToString(),i.ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = ddl.Items.Count - 1;
            //if (i > 0)
            //{
            //    for (int i = 0; i <= ddl.Items.Count - 1; i++)
            //    {
            //        if (ddl.Items[i].Value.ToString() == i.ToString())
            //        {
            //            ddl.SelectedIndex = i;
            //            break; // TODO: might not be correct. Was : Exit For
            //        }
            //    }
            //}
        }
        public static byte[] StreamToByte(System.IO.Stream Stream)
        {
            Int32 length = Stream.Length > Int32.MaxValue ? Int32.MaxValue : Convert.ToInt32(Stream.Length);
            Byte[] buffer = new Byte[length];
            Stream.Read(buffer, 0, length);
            return buffer;
        }
    }
}