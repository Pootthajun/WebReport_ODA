﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TICA_ODA_WebReport
{
    public partial class RPT_Finance_project : System.Web.UI.Page
    {
        double result = 0.00;
        double n1 = 0.00;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Class1.BindDDlYear(ddlBubgetYear);
               
            }
        }
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindData();
            if (ddlBubgetYear.SelectedIndex ==0)
            {
                lblHeadTable.Text = "ทุกปี";
            }
            else
            {
                lblHeadTable.Text = "  ปี " + ddlBubgetYear.Text + "";
            }  
            
        }

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            Label lblList = (Label)e.Item.FindControl("lblList");
            Label lblBudget = (Label)e.Item.FindControl("lblBudget");
            Label lblAllocateBudget = (Label)e.Item.FindControl("lblAllocateBudget");
            Label lblDisburse = (Label)e.Item.FindControl("lblDisburse");
            Label lblBalance = (Label)e.Item.FindControl("lblBalance");
            Label lblBudgetYear = (Label)e.Item.FindControl("lblBudgetYear");
            Label lblResultBudget = (Label)e.Item.FindControl("lblResultBudget");
           // Label lblResulBlance =(Label)e.Item.FindControl("lblResulBlance");
            DataRowView drv = (DataRowView)e.Item.DataItem;

           

            lblList.Text = drv["country_name"].ToString();
            if (drv["project_budget"].ToString() !="")
            {
                
                lblBudget.Text = Class1.StringFormatNumber(drv["project_budget"].ToString(), 2);
            }
            else
            {
                lblBudget.Text = "0.00";
            }
            if (drv["dis_budget"].ToString() != "")
            {

                lblAllocateBudget.Text = Class1.StringFormatNumber(drv["dis_budget"].ToString(), 2);

               // Convert.ToDouble(drv["dis_budget"]).ToString("#,##0.00");

                //DateTime.Now.ToString("dd/MMMM/yyyy", new System.Globalization.CultureInfo("th-TH") )
            }
            else
            {
                lblAllocateBudget.Text = "0.00";
            }
            if (drv["mp_amount"].ToString() != "")
            {

                lblDisburse.Text = Class1.StringFormatNumber(drv["mp_amount"].ToString(), 2);
            }
            else
            {
                lblDisburse.Text = "0.00";
            }
            if (drv["mp_year"].ToString() !="")
            {
                lblBudgetYear.Text = drv["mp_year"].ToString();
            }
            else
            {
                lblBudgetYear.Text = "";
            }
            double project_budget = Class1.CDBL(Class1.StringFormatNumber(drv["project_budget"].ToString(), 2));
            double dis_budget = Class1.CDBL(Class1.StringFormatNumber(drv["dis_budget"].ToString(), 2));
            double mp_amount = Class1.CDBL(Class1.StringFormatNumber(drv["mp_amount"].ToString(), 2));
            result = project_budget - (dis_budget + mp_amount);
            lblBalance.Text = Class1.StringFormatNumber(result.ToString(), 2);
           
            

                    n1 += result;
                

            
            string strSumbalance = n1.ToString();
            lblResulBlance.Text = Class1.StringFormatNumber(strSumbalance.ToString(), 2);
            //if (drv["Balance"].ToString() != "")
            //{

            //    lblBalance.Text = Class1.StringFormatNumber(drv["Balance"].ToString(), 2);
            //}
            //else
            //{
            //    lblBalance.Text = "0.00";
            //}



        }
        private void BindData()
        {


            //string SQL = "SELECT Budget.project_id,Budget.project_name,SUM(Budget.project_budget) as project_budget,"+ "\n";
            //       SQL +=" SUM(Budget.dis_budget) as dis_budget,Budget.country_name,SUM(Project_Paid.mp_amount) as mp_amount,"+ "\n";
            //       SQL += " CONVERT(Project_Paid.mp_year,CHAR(50)) as mp_year FROM Budget LEFT JOIN Project_Paid ON Budget.project_id = Project_Paid.project_id" + "\n";

            string BudgetYear = "";
            string Filter = "";
            if (txtSearch.Text != "")
            {
                Filter += " (country_name like '%" + txtSearch.Text + "%')  AND" + "\n";
            }

            if (ddlBubgetYear.Text != "")
            {
                if (ddlBubgetYear.SelectedIndex == 0)
                {

                    Filter += " 1=1  AND";
                    BudgetYear = "รายงานการจัดสรรงบประมาณรายประเทศทั้งหมด";


                }
                else
                {
                    Filter += " mp_year='" + ddlBubgetYear.SelectedValue + "'" + "  AND" + "\n";
                    BudgetYear = "รายงานการจัดสรรงบประมาณรายประเทศประจำปี " + ddlBubgetYear.Text + "";
                }

            }
            if (!string.IsNullOrEmpty(Filter))
            {
                Filter += "  " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }

            DataTable DT = new DataTable();
            DataSet DS = new DataSet();

            //DT = Class1.ExecuteTable(SQL);
            //DT.TableName = "DataTable";
            //ds.Tables.Add(DT);
            //ds.DataSetName = "Dataset";
            //ds.WriteXml("E:\\Test\\qry.xml",XmlWriteMode.WriteSchema);
            string tmpXML = "C:\\webroot\\oda\\TempXML\\";
            if (System.IO.Directory.Exists(tmpXML) == false)
                System.IO.Directory.CreateDirectory(tmpXML);

            string FileName = tmpXML + "qry_project.xml";

            if (System.IO.File.Exists(FileName) == true)
            {
                System.IO.File.SetAttributes(FileName, System.IO.FileAttributes.Normal);
                System.IO.File.Delete(FileName);
            }


            string strXML = ODA_WebService.GetDataFromURL("http://oda.mfa.go.th/query/query_project.php", "", ODA_WebService.ContentType.Text, ODA_WebService.FormatReturnData.XML);
            if (strXML.Trim() != "")
            {
                strXML = ODA_WebService.StringReplace(strXML, "&", "&amp;");

                System.IO.File.WriteAllText(FileName, strXML);
            }
            if (System.IO.File.Exists(FileName) == true)
            {
                DS.ReadXml(FileName);
                DT = DS.Tables[0];
                DT.DefaultView.RowFilter = Filter;
                double sumPBudget= 0.00;
                double sumDisburse = 0.00;
                double sumPPaid = 0.00;
                double sumBalance = 0.00;
                if (DT.DefaultView.Count > 0)
                {

                    for (int i = 0; i < DT.DefaultView.Count; i++)
                    {
                        if (DT.DefaultView[i]["project_budget"].ToString() != "")
                        {

                            sumPBudget = sumPBudget + Class1.CDBL(Class1.StringFormatNumber(DT.DefaultView[i]["project_budget"].ToString(), 2));
                        }

                    }
                    if (sumPBudget > 0)
                    {
                        lblResultBudget.Text = Class1.StringFormatNumber(sumPBudget, 2);
                    }
                    else
                    {
                        lblResultBudget.Text = "0.00";
                    }
                }
                else
                {
                    lblResultBudget.Text = "0.00";
                }

                if (DT.DefaultView.Count > 0)
                {

                    for (int i = 0; i < DT.DefaultView.Count; i++)
                    {
                        if (DT.DefaultView[i]["dis_budget"].ToString() != "")
                        {

                            sumDisburse = sumDisburse + Class1.CDBL(Class1.StringFormatNumber(DT.DefaultView[i]["dis_budget"].ToString(), 2));
                        }

                    }
                    if (sumDisburse > 0)
                    {
                        lblResultAllocateBudget.Text = Class1.StringFormatNumber(sumDisburse, 2);
                    }
                    else
                    {
                        lblResultAllocateBudget.Text = "0.00";
                    }
                }
                else
                {
                    lblResultAllocateBudget.Text = "0.00";
                }

                if (DT.DefaultView.Count > 0)
                {

                    for (int i = 0; i < DT.DefaultView.Count; i++)
                    {
                        if (DT.DefaultView[i]["mp_amount"].ToString() != "")
                        {

                            sumPPaid = sumPPaid + Class1.CDBL(Class1.StringFormatNumber(DT.DefaultView[i]["mp_amount"].ToString(), 2));
                        }

                    }
                    if (sumDisburse > 0)
                    {
                        lblResultDisburse.Text = Class1.StringFormatNumber(sumPPaid, 2);
                    }
                    else
                    {
                        lblResultDisburse.Text = "0.00";
                    }
                }
                else
                {
                    lblResultDisburse.Text = "0.00";
                }

                if (DT.DefaultView.Count > 0)
                {
                    lblShowCountData.Text = "พบ " + Class1.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
                }
                else
                {
                    lblShowCountData.Text = "ไม่พบรายการ";
                }
              
                rptList.DataSource = DT.DefaultView.ToTable();
                rptList.DataBind();


                Session["RPT_Finance_project"] = DT.DefaultView.ToTable();
                Session["RPT_Finance_project_BudgetYear"] = BudgetYear;

                Session["RPT_Finance_project_lblResultBudget"] = lblResultBudget.Text;
                Session["RPT_Finance_project_lblResultAllocateBudget"] = lblResultAllocateBudget.Text;
                Session["RPT_Finance_project_lblResultDisburse"] = lblResultDisburse.Text;
                Session["RPT_Finance_project_lblResulBlance"] = lblResulBlance.Text;
               
            }
        }
    }
}