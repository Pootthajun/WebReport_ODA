﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TICA_ODA_WebReport
{
    public partial class RPT_Finance_Recipient1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Class1.BindDDlYear(ddlBubgetYear);
                BindData();
            }
           
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindData();

            if (txtDateStart.Text != "" && txtDateTo.Text != "")
            {
                lblHeadTable.Text = "  วันที่ " + txtDateStart.Text + " - " + txtDateTo.Text + " ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            if (txtDateStart.Text == "")
            {
                lblHeadTable.Text = "  วันที่ " + txtDateTo.Text + " ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            if (txtDateTo.Text == "")
            {
                lblHeadTable.Text = "  วันที่ " + txtDateStart.Text + " ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            if (txtDateStart.Text == "" && txtDateTo.Text == "")
            {
                lblHeadTable.Text = "  ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            if (ddlBubgetYear.SelectedIndex ==0)
            {
                if (txtDateStart.Text !="" && txtDateTo.Text !="")
                {
                    lblHeadTable.Text = "  วันที่ " + txtDateStart.Text + " - " + txtDateTo.Text + " ทุกปีงบประมาณ";
                }
                else
                {
                    lblHeadTable.Text = " ทุกปีงบประมาณ";
                }
               
            }
            else
            {
                lblHeadTable.Text = "  วันที่ " + txtDateStart.Text + " - " + txtDateTo.Text + " ปีงบประมาณ " + ddlBubgetYear.Text + "";
            }
            //lblHeadTable.Text = "  วันที่ "+txtDateStart.Text+" - "+txtDateTo.Text+" ปีงบประมาณ "+ddlBubgetYear.Text+"";
        }
        private void BindData()
        {


            //string SQL = "SELECT project.project_id,project.project_name,project.project_object,project.project_start,project.project_end,project.project_budget,activity.activity_id,activity.activity_name,aid.aid_id,aid.aid_budget,aid.aid_name,money_loan.ml_id,money_loan.aid_id,money_loan.rec_id,money_loan.loan_number,money_loan.ml_amount,money_loan.ml_year,money_loan.ml_date,money_loan.ml_date_back,recipient.rec_id,recipient.aid_id,recipient.prefix_id,recipient.fullname FROM project INNER JOIN activity ON project.project_id=activity.project_id INNER JOIN aid ON activity.activity_id=aid.activity_id INNER JOIN money_loan ON aid.aid_id= money_loan.aid_id INNER JOIN recipient  ON money_loan.rec_id= recipient.rec_id";
            double result = 0.00;
            string Filter = "";
            if (txtSearch.Text != "")
            {
                Filter += "(aid_name like '%" + txtSearch.Text + "%' OR fullname like '%" + txtSearch.Text + "%' OR loan_number like '%" + txtSearch.Text + "%')  AND" + "\n";
            }
            if (txtDateStart.Text != "" && txtDateTo.Text != "")
            {
                //Convert.ToDateTime(txtDateTo.Text).ToString("dd-MM-", new System.Globalization.CultureInfo("en-US")) + (Class1.CINT(Convert.ToDateTime(txtDateTo.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString();
                string StartDate = (Class1.CINT(Convert.ToDateTime(txtDateStart.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString() + Convert.ToDateTime(txtDateStart.Text).ToString("-MM-dd", new System.Globalization.CultureInfo("en-US"));
                string EndDate   = (Class1.CINT(Convert.ToDateTime(txtDateTo.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString() + Convert.ToDateTime(txtDateTo.Text).ToString("-MM-dd", new System.Globalization.CultureInfo("en-US"));

                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                System.Threading.Thread.CurrentThread.CurrentCulture = ci;

              //  string sd = Convert.ToDateTime(txtDateStart.Text).ToString("yyyy/MM/dd");
               // string ed = Convert.ToDateTime(txtDateTo.Text).ToString("yyyy/MM/dd");

                Filter += " ml_date>='" + StartDate + "' AND ml_date<='" + EndDate + "'  AND" + "\n";

                //Filter += " ml_date>='" + StartDate + "' AND ml_date<='" + EndDate + "'  AND" + "\n";
            }
            else
            {
                if (txtDateStart.Text != "")
                {
                    string StartDate = (Class1.CINT(Convert.ToDateTime(txtDateStart.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString() + Convert.ToDateTime(txtDateStart.Text).ToString("-MM-dd", new System.Globalization.CultureInfo("en-US"));
                    Filter += " ml_date>='" + StartDate + "'  AND" + "\n";
                }
                if (txtDateTo.Text != "")
                {
                    string EndDate = (Class1.CINT(Convert.ToDateTime(txtDateTo.Text).ToString("yyyy", new System.Globalization.CultureInfo("en-US"))) + Class1.CINT(543)).ToString() + Convert.ToDateTime(txtDateTo.Text).ToString("-MM-dd", new System.Globalization.CultureInfo("en-US"));
                    Filter += " ml_date<='" + EndDate + "'  AND" + "\n";
                }
            }
           
            if (ddlBubgetYear.Text !="0")
            {
                Filter += " ml_year='" + ddlBubgetYear.SelectedValue + "'" + "  AND" + "\n";
            }
            if (!string.IsNullOrEmpty(Filter))
            {
                Filter = "  " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }






            DataTable DT = new DataTable();
            DataSet DS = new DataSet();

            //DT = Class1.ExecuteTable(SQL);
            //DT.TableName = "DataTable";
            //ds.Tables.Add(DT);
            //ds.DataSetName = "Dataset";
            //ds.WriteXml("E:\\Test\\qry.xml",XmlWriteMode.WriteSchema);
            string tmpXML = "C:\\webroot\\oda\\TempXML\\";
            if (System.IO.Directory.Exists(tmpXML) == false)
                System.IO.Directory.CreateDirectory(tmpXML);

            string FileName = tmpXML + "qry_recipient.xml";

            if (System.IO.File.Exists(FileName) == true)
            {
                System.IO.File.SetAttributes(FileName, System.IO.FileAttributes.Normal);
                System.IO.File.Delete(FileName);
            }


            string strXML = ODA_WebService.GetDataFromURL("http://oda.mfa.go.th/query/query_recipient.php", "", ODA_WebService.ContentType.Text, ODA_WebService.FormatReturnData.XML);
            if (strXML.Trim() != "")
            {
                strXML = ODA_WebService.StringReplace(strXML, "&", "&amp;");

                System.IO.File.WriteAllText(FileName, strXML);
            }


            if (System.IO.File.Exists(FileName) == true)
            {
                DS.ReadXml(FileName);
                DT = DS.Tables[0];
                DT.DefaultView.RowFilter = Filter;
                if (DT.DefaultView.Count > 0)
                {
                    
                    for (int i = 0; i < DT.DefaultView.Count; i++)
                    {
                        if (DT.DefaultView[i]["ml_amount"].ToString() != "")
                        {

                            result = result + Class1.CDBL(Class1.StringFormatNumber(DT.DefaultView[i]["ml_amount"].ToString(), 2));
                        }

                    }
                    //Object result = DT.Compute("SUM(ml_amount)", "ml_amount IS NOT NULL");
                    if (result > 0)
                    {

                        lblResult.Text = Class1.StringFormatNumber(result, 2);
                    }
                    else
                    {
                        lblResult.Text = "0.00";
                    }
                }
            }
            else
            {
                lblResult.Text = "0.00";
            }

            if (DT.DefaultView.Count > 0)
            {
                lblShowCountData.Text = "พบ " + Class1.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
            }
            else
            {
                lblShowCountData.Text = "ไม่พบรายการ";
            }
            rptList.DataSource = DT.DefaultView.ToTable();
            rptList.DataBind();
             string BudgetYear = "";
            if (ddlBubgetYear.SelectedIndex == 0)
            {
                 BudgetYear = "ทุกปีงบประมาณ";
            }
            else
            {
                 BudgetYear = "ปีงบฯ " + ddlBubgetYear.Text + "";
            }
            Session["RPT_Finance_Recipient"] = DT.DefaultView.ToTable();
           
            string DateRang = "";
            if (txtDateStart.Text !="" && txtDateTo.Text !="")
            {
                 DateRang = "วันที่ " + txtDateStart.Text + " - " + txtDateTo.Text + "";
            }
            else
            {
                if (txtDateStart.Text!="")
                {
                    DateRang = "วันที่ " + txtDateStart.Text + "";
                }
                if (txtDateTo.Text !="")
                {
                    DateRang = " วันที่ " + txtDateTo.Text + "";
                }
            }
            Session["RPT_Finance_Recipient_BudgetYear"] = BudgetYear;
            Session["RPT_Finance_Recipient_DateRang"] = DateRang;
            Session["RPT_Finance_Recipient_sumMLAmount"] = result.ToString();

        }

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            Label lblName = (Label)e.Item.FindControl("lblName");
            Label lblObject = (Label)e.Item.FindControl("lblObject");
            Label lblDateLoan = (Label)e.Item.FindControl("lblDateLoan");
            Label lblDateBack = (Label)e.Item.FindControl("lblDateBack");
            Label lblAmount = (Label)e.Item.FindControl("lblAmount");
            Label lblNo = (Label)e.Item.FindControl("lblNo");


            DataRowView drv = (DataRowView)e.Item.DataItem;

            //if (LastSector != drv["SECTOR_NAME"].ToString())
            //{
            //    lblSector.Text = drv["SECTOR_NAME"].ToString();
            //    LastSector = drv["SECTOR_NAME"].ToString();
            //}

            lblName.Text = drv["fullname"].ToString();
            lblObject.Text = drv["aid_name"].ToString();
            if (drv["ml_date"].ToString() != "")
            {
                //yyyy-MM-dd
                string[] mlDate = drv["ml_date"].ToString().Split('-');
                if (mlDate.Length == 3) {
                    lblDateLoan.Text = mlDate[2] + "/" + mlDate[1] + "/" + mlDate[0];
                }

                
            }
            else
            {
                lblDateLoan.Text = "";
            }
            if (drv["ml_date_back"].ToString() != "")
            {
                lblDateBack.Text = Class1.DateTimeToThaiDateTime((DateTime)drv["ml_date_back"]);
            }
            else
            {
                lblDateBack.Text = "";
            }
            lblAmount.Text = Class1.StringFormatNumber(drv["ml_amount"].ToString(), 2);
            lblNo.Text = drv["loan_number"].ToString();
        }
    }
}