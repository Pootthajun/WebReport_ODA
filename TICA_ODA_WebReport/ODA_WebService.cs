﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace TICA_ODA_WebReport
{
    public class ODA_WebService
    {
        public enum ContentType
        {
            Default = 1,
            Jason = 2,
            Text = 3
        }
        public enum FormatReturnData
        {
            XML = 1,
            Jason = 2,
            SSB_Olympus = 3,
            SGSCP_QueryData = 4,
            SGSCP_Topup = 5,
            SGSCP_CancelTopup = 6
        }

        public static string GetWebserviceContentType(ContentType ContentType)
        {
            switch (ContentType)
            {
                case ContentType.Default:
                    return "application/x-www-form-urlencoded";
                case ContentType.Jason:
                    return "application/json; charset=utf-8";
                case ContentType.Text:
                    return "text/xml; charset=utf-8";
                default:
                    return "";
            }
        }
        public static string GetDataFromURL(string URL, string Parameter, ContentType ContentType, FormatReturnData FormatReturnData)
        {
            string ret = "";
             WebRequest request = default(WebRequest);
            request = WebRequest.Create(URL);
            request.Timeout = 10000;
            WebResponse response = default(WebResponse);
            response = request.GetResponse();

            StreamReader stream = new StreamReader(response.GetResponseStream(), System.Text.UnicodeEncoding.UTF8);
            if (stream.Peek() != -1) {
                ret = stream.ReadToEnd();
            }

            return ret;

            //byte[] data = Encoding.UTF8.GetBytes(Parameter);
            //request.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            //request.Method = "POST";
            //request.ContentType = GetWebserviceContentType(ContentType);
            //request.ContentLength = data.Length;
            //Stream stream = request.GetRequestStream();
            //stream.Write(data, 0, data.Length);
            //stream.Close();
            //response = request.GetResponse();
            //return ;
        }
        public static string StringReplace(string strXML,string str,string strRep) {

            string ret = "";
            ret = strXML.Replace(str,strRep);
            return ret;
        }
    }
}