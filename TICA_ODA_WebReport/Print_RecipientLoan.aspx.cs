﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;

namespace TICA_ODA_WebReport
{
    public partial class Print_RecipientLoan : System.Web.UI.Page
    {
        ReportDocument cc;
        Converter C = new Converter();
        protected void Page_Load(object sender, EventArgs e)
        {
           
           
            cc = new ReportDocument();
            cc.Load(Server.MapPath("Report/RPT_Finance_Recipient.rpt"));
            //cc.Subreports["C_BHV"].SetDataSource(BT);
            DataTable DT = (DataTable)Session["RPT_Finance_Recipient"];
            cc.SetDataSource(DT);
            CrystalReportViewer1.ReportSource = cc;
            cc.SetParameterValue("BudgetYear", Session["RPT_Finance_Recipient_BudgetYear"].ToString());
            cc.SetParameterValue("DateRang", Session["RPT_Finance_Recipient_DateRang"].ToString());
            cc.SetParameterValue("sumMLAmount", Session["RPT_Finance_Recipient_sumMLAmount"].ToString());
            //cc.SetParameterValue("SumMlAmount", Session["RPT_Finance_Recipient_SumMlAmount"].ToString());


            byte[] B;
            switch (Request.QueryString["Mode"].ToUpper())
            {
                case "PDF":
                    Response.AddHeader("Content-Type", "application/pdf");
                    Response.AppendHeader("Content-Disposition", "filename=รายงานลูกหนี้เงินทดรองบัญชีอุดหนุนให้ฯ(เพื่อนบ้าน)_" + DateTime.Now.Year + DateTime.Now.Month.ToString().PadLeft(2, Class1.chr0) + DateTime.Now.Day.ToString().PadLeft(2, Class1.chr0) +".pdf");
                    B = Class1.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
                    Response.BinaryWrite(B);
                    break;
                case "EXCEL":
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AppendHeader("Content-Disposition", "filename=รายงานลูกหนี้เงินทดรองบัญชีอุดหนุนให้ฯ(เพื่อนบ้าน)_" + DateTime.Now.Year + DateTime.Now.Month.ToString().PadLeft(2, Class1.chr0) + DateTime.Now.Day.ToString().PadLeft(2, Class1.chr0) + ".xls");
                    B = Class1.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
                    Response.BinaryWrite(B);
                    break;
                default:
                    break;

            }
        }
    }
}