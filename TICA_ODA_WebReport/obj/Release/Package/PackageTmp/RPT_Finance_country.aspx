﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RPT_Finance_country.aspx.cs" Inherits="TICA_ODA_WebReport.RPT_Finance_country" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderCSS" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderPage" runat="server">

        <!-- Page -->
<div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">รายงานการใช้จ่ายเงินอุดหนุน</h1>
    </div>
    <div class="page-content">
      <div class="panel">
      
        <div class="panel-body">


      <!-- Panel Inline Form -->
          <br/>
          

         
              <div class="form-inline">
                <div class="form-group">
                  
                  <label class="control-label" for="inputInlineUsername">ค้นหา</label>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" placeholder="ค้นหาจาก ชื่อประเทศ ชื่อรายการ" Width="300px"></asp:TextBox>
                </div>
                     <label class="control-label" for="inputInlinePassword">ปีงบประมาณ</label>
                  <div class="checkbox-custom checkbox-default">
                       <div class="example">
                 <asp:DropDownList ID="ddlBubgetYear" runat="server" CssClass="form-control">
                      <asp:ListItem>2559</asp:ListItem>
                           </asp:DropDownList>
                </div>
                  </div>
                                     <asp:Button ID="btn_Search" runat="server" Text="ค้นหา" CssClass="btn btn-primary btn-outline" OnClick="btn_Search_Click"/>
  
                <div class="form-group">
                    
       
                 
                                    <div class="input-group">
                                        <span class="input-group-addon">ระหว่างวันที่
                                            <i class="icon wb-calendar" aria-hidden="true"></i>
                                        </span>
                                        <asp:TextBox ID="txtDateStart" runat="server" CssClass="form-control" data-plugin="datepicker" data-date-format="dd/mm/yyyy"></asp:TextBox>
                                        <span class="input-group-addon">   ถึง
                                            <i class="icon wb-calendar" aria-hidden="true"></i>
                                        </span>
                                        <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" data-plugin="datepicker" data-date-format="dd/mm/yyyy"></asp:TextBox>
                                    </div>
                                                                
                                
                    </div>
                </div>          
     

          <!-- End Panel Inline Form -->
         <h4>รายงานการใช้จ่ายเงินอุดหนุน<asp:Label ID="lblHeadTable" runat="server" Text=""></asp:Label></h4>
                      <div>
        
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-2"> <button data-toggle="dropdown" class="btn dropdown-toggle btn-primary" id="Button1">พิมพ์รายงาน <i class="icon-angle-down"></i></button>          
                        <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a href="Print_Finance_country.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print_Finance_country.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>           
                   </ul></div>
    </div>
          <div>
          <CENTER><h4><asp:Label ID="lblShowCountData" runat="server" Text=""></asp:Label></h4></CENTER>
             </div>
               <table class="table toggle-arrow-tiny table-striped dataTable" id="examplePagination" data-page-size="20">
                        <thead>
                            <tr>
                                <th>ประเทศ</th>
                                <th>รายการ</th>
                                <th>ระยะเวลา</th>
                                <th>งบประมาณที่จัดสรร</th>
                                <th>เงินยืมทดรองฯ/เงินจ่ายล่วงหน้า</th>
                                <th>ค่าใช้จ่ายที่เบิกจ่ายแล้ว</th>
                                <th>งบประมาณคงเหลือ</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>รวม</th>
                                <th><asp:Label ID="lblAllocate" runat="server" Text="Label"></asp:Label></th>
                                <th><asp:Label ID="lblLoan" runat="server"></asp:Label></th>
                                <th><asp:Label ID="lblPaid" runat="server" Text="Label"></asp:Label></th>
                                <th><asp:Label ID="lblSumBalance" runat="server" Text=""></asp:Label></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <asp:Repeater ID="rptList"  runat="server" OnItemDataBound="rptList_ItemDataBound">
										<ItemTemplate>        
                            <tr>
                                <td><asp:Label ID="lblCountry" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblList" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblDateStart" runat="server"></asp:Label> - <asp:Label ID="lblDateTo" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblAllocate" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblLoan" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblDisburse" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblBalance" runat="server" Text=""></asp:Label></td>
                            </tr>
                            </ItemTemplate>
                           </asp:Repeater>
                        </tbody>
                    </table>                         
          <div class="panel-body">
        <div class="col-md-6"></div>
   
        <div class="col-md-6"><div>
                                                <ul class="pagination"></ul>
                                            </div> </div>
    </div> 
      
              
      


        </div>
      
        </div>
    </div> 
</div>   
  <!-- End Page -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScript" runat="server">
</asp:Content>
