﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Print_RecipientLoan.aspx.cs" Inherits="TICA_ODA_WebReport.Print_RecipientLoan" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>รายงานลูกหนี้เงินทดรองบัญชีอุดหนุนให้ฯ (เพื่อนบ้าน)</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
    </div>
    </form>
</body>
</html>
