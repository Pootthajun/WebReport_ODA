﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;

namespace TICA_ODA_WebReport
{
    public class ODAMySQLDb
    {
        public static string GetConnectionString()
        {
            string ret = "";

            try
            {
                //DatabaseConfigData db = OPMSqlDb.getDatebaseConfig("MySqlDPIS");
               
                    ret = "server=127.0.0.1;port=81;uid=root;database=oda_db;";
                
            }
            catch (Exception ex)
            {
                ret = "";
            }
            return ret;
        }

        private static MySqlConnection GetConnection()
        {
            string connStr = GetConnectionString();
            MySqlConnection conn = null;
            try {
                
                //conn = MySqlConnectDB.GetConnection(connStr);
                 conn = new MySqlConnection(connStr);
            }
            catch (Exception ex) {
                conn = null;
            }
            return conn;
        }


        public static DataTable ExecuteTable(string sql)
        {
            MySqlCommand cmd = new MySqlCommand();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            adapter.SelectCommand = cmd;
            DataTable dt = new DataTable();

            try
            {
                MySqlConnection conn = GetConnection();

                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;

                adapter.Fill(dt);
                adapter.Dispose();

                cmd.Dispose();
                conn.Close();
            }
            catch (Exception ex)
            {
                dt = new DataTable();
            }
            return dt;
        }

        public static string ExecuteNonQuery(string sql)
        {
            string ret = "false";
            try
            {
                MySqlConnection conn = GetConnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;
                cmd.ExecuteNonQuery();

                conn.Close();
                cmd.Dispose();

                ret = "true";
            }
            catch (Exception ex)
            {
                ret = "false|Exception : " + ex.Message + Environment.NewLine + ex.StackTrace;
            }
            return ret;
        }

        public static string ExecuteNonQuery(string sql, MySqlParameter[] param)
        {
            string ret = "false";
            try
            {
                MySqlConnection conn = GetConnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;

                if (param != null)
                {
                    foreach (MySqlParameter p in param)
                    {
                        if (p != null)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }
                }

                cmd.ExecuteNonQuery();
                ret = "true";
            }
            catch (Exception ex)
            {
                ret = "false|Exception " + ex.Message + "\n" + ex.StackTrace.ToString();
            }

            return ret;
        }
        
        public static DataTable ExecuteTable(string sql, MySqlTransaction trans)
        {
            return ExecuteTable(sql, trans);
        }

        public static DataTable ExecuteTable(string sql, MySqlParameter[] param)
        {
            MySqlCommand cmd = new MySqlCommand();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            adapter.SelectCommand = cmd;
            DataTable dt = new DataTable();

            try
            {
                MySqlConnection conn = GetConnection();

                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 240;

                if (param != null)
                {
                    foreach (MySqlParameter p in param)
                    {
                        if (p != null)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }
                }

                adapter.Fill(dt);
                adapter.Dispose();

                cmd.Dispose();
                conn.Close();
            }
            catch (Exception ex)
            {
                dt = new DataTable();
            }
            return dt;
        }

        public static bool ExecuteNonQuery(string sql, MySqlTransaction trans)
        {
            return ExecuteNonQuery(sql, trans);
        }

        private static MySqlParameter setParameter(string pName, MySqlDbType pType, object pValue)
        {
            MySqlParameter p = new MySqlParameter(pName, pType);
            p.Value = pValue;

            return p;
        }

        public static MySqlParameter setText(string pName, string pValue)
        {
            return setParameter(pName, MySqlDbType.Text, pValue);
        }
        public static MySqlParameter setDouble(string pName, object pValue) {
            return setParameter(pName, MySqlDbType.Double, pValue);
        }

        public static MySqlParameter setBigint(string pName, object pValue)
        {
            return setParameter(pName, MySqlDbType.Int64, pValue);
        }
        public static MySqlParameter setInt(string pName, object pValue)
        {
            return setParameter(pName, MySqlDbType.Int16, pValue);
        }
        public static MySqlParameter setDateTime(string pName, DateTime pValue) {
            return setParameter(pName, MySqlDbType.DateTime, pValue);
        }
            

        public static bool CheckConnection()
        {
            bool ret = false;
            try
            {
                MySqlConnection conn = GetConnection();
                if (conn.State==ConnectionState.Open)
                    ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }
    }
}
