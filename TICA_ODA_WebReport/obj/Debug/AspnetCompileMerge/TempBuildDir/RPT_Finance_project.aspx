﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RPT_Finance_project.aspx.cs" Inherits="TICA_ODA_WebReport.RPT_Finance_project" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderCSS" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderPage" runat="server">

        <!-- Page -->
<div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">รายงานการจัดสรรงบประมาณรายประเทศประจำปี</h1>
    </div>
    <div class="page-content">
      <div class="panel">
      
        <div class="panel-body">
      <div class="page-content">

      <!-- Panel Inline Form -->
          <br/>
          
          <div class="example-wrap">
            <div class="example">
              <div class="form-inline">
                <div class="form-group">
                  <label class="control-label" for="inputInlineUsername">ค้นหา</label>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" placeholder="ค้นหาจาก ชื่อประเทศ" Width="300px"></asp:TextBox>
                </div>
                <div class="form-group">
                
                </div>
                <div class="form-group">
                    <label class="control-label" for="inputInlinePassword">ปีงบประมาณ</label>
                  <div class="checkbox-custom checkbox-default">
                       <div class="example">
                 <asp:DropDownList ID="ddlBubgetYear" runat="server" CssClass="form-control">
                      <asp:ListItem>2559</asp:ListItem>
                           </asp:DropDownList>
                </div>
                  </div>
                </div>
                 <asp:Button ID="btn_Search" runat="server" Text="ค้นหา" CssClass="btn btn-primary btn-outline" OnClick="btn_Search_Click"/>
              </div>
                <div class="form-group">
                 
           
                   </div>
            </div>
          </div>
          <!-- End Panel Inline Form -->
         <h4>รายงานข้อมูลการจัดสรรงบประมาณรายประเทศ<asp:Label ID="lblHeadTable" runat="server" Text=""></asp:Label></h4>
          <div>
          <CENTER><h4><asp:Label ID="lblShowCountData" runat="server" Text=""></asp:Label></h4></CENTER>
             </div>
               <table class="table toggle-arrow-tiny table-striped dataTable" id="examplePagination" data-page-size="20">
                        <thead>
                            <tr>
                                <th>รายการ</th>
                                <th>ปีงบประมาณ</th>
                                <th>งบประมาณ</th>
                                <th>จัดสรรแล้ว</th>
                                <th>เบิกจ่าย</th>
                                <th>คงเหลือ</th>
                               
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>รวม</th>
                                <th><asp:Label ID="lblResultBudget" runat="server"></asp:Label></th>
                                <th><asp:Label ID="lblResultAllocateBudget" runat="server"></asp:Label></th>
                                <th><asp:Label ID="lblResultDisburse" runat="server"></asp:Label></th>
                                <th><asp:Label ID="lblResulBlance" runat="server"></asp:Label></th>
                                 <th></th>
                                
                                
                            </tr>
                        </tfoot>
                        <tbody>
                            <asp:Repeater ID="rptList"  runat="server" OnItemDataBound="rptList_ItemDataBound">
										<ItemTemplate>        
                            <tr>
                                <td><asp:Label ID="lblList" runat="server"></asp:Label></td>
                                <th><asp:Label ID="lblBudgetYear" runat="server"></asp:Label></th>
                                <td><asp:Label ID="lblBudget" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblAllocateBudget" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblDisburse" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblBalance" runat="server"></asp:Label></td>
                                
                            </tr>
                            </ItemTemplate>
                           </asp:Repeater>
                        </tbody>
                    </table>                         
   <div class="panel-body">
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"> <button data-toggle="dropdown" class="btn dropdown-toggle btn-primary" id="Button1">พิมพ์รายงาน <i class="icon-angle-down"></i></button>          
                        <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a href="Print_Finance_project.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print_Finance_project.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>           
                   </ul></div>
    </div>
      
              
      
    </div>

        </div>
      
        </div>
    </div> 
</div>   
  <!-- End Page -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScript" runat="server">
</asp:Content>
