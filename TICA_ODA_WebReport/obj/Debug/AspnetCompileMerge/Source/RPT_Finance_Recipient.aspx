﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RPT_Finance_Recipient.aspx.cs" Inherits="TICA_ODA_WebReport.RPT_Finance_Recipient1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderCSS" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderPage" runat="server">

     <!-- Page -->
<div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">รายงานลูกหนี้เงินทดรองบัญชีอุดหนุนให้ฯ (เพื่อนบ้าน)</h1>
    </div>
    <div class="page-content">
      <div class="panel">
      
        <div class="panel-body">
      <div class="page-content">

      <!-- Panel Inline Form -->
          <br/>
           <br/>
           <br/>
          
          <div class="example-wrap">
            <div class="example">
              <div class="form-inline">
                <div class="form-group">
                  <label class="control-label" for="inputInlineUsername">ค้นหา</label>
                    <asp:TextBox ID="txtSearch" runat="server"  CssClass="form-control" placeholder="ค้นหาจาก ชื่อผู้ยืม วัตถุประสงค์ เลขที่ใบยืม" Width="300px"></asp:TextBox>
                </div>
                  
              <div class="form-group">
                   <div class="example-wrap">
                                <div class="example">
                                    <div class="input-group">
                                        <span class="input-group-addon">วันที่ยืม
                                            <i class="icon wb-calendar" aria-hidden="true"></i>
                                        </span>
                                        <asp:TextBox ID="txtDateStart" runat="server" CssClass="form-control" data-plugin="datepicker" data-date-format="yyyy/mm/dd" ></asp:TextBox>
                                        <span class="input-group-addon">   ถึง
                                            <i class="icon wb-calendar" aria-hidden="true"></i>
                                        </span>
                                        <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" data-plugin="datepicker" data-date-format="yyyy/mm/dd" ></asp:TextBox>
                                    </div>
                                </div>
                                
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label" for="inputInlinePassword">ปีงบประมาณ</label>
                  <div class="checkbox-custom checkbox-default">
                       <div class="example">
                 <asp:DropDownList ID="ddlBubgetYear" runat="server" CssClass="form-control">
                      <asp:ListItem></asp:ListItem>
                           </asp:DropDownList>
                </div>
                  </div>
                </div>
                 <asp:Button ID="btn_Search" runat="server" Text="ค้นหา" CssClass="btn btn-primary btn-outline" OnClick="btn_Search_Click"/>
              </div>
                <div class="form-group">
                   </div>
            </div>
          </div>
          <!-- End Panel Inline Form -->
         <h4>รายงานข้อมูลลูกหนี้เงินทดรองบัญชีอุดหนุนให้ฯ (เพื่อนบ้าน)<asp:Label ID="lblHeadTable" runat="server" Text=""></asp:Label></h4>
          <div>
          <CENTER><h4 style="color:black;background-color:lightblue"><B><asp:Label ID="lblShowCountData" runat="server" Text=""></asp:Label></B></h4></CENTER>
             </div>
            <div class="col-lg-12">
                    <!-- Panel Pagination -->
                    <div class="panel">
                        <header class="panel-heading">
                            <h3 class="panel-title"></h3>
                        </header>
                     
                            <label class="form-inline">
                                Show
                                <select id="exampleShow" class="form-control input-sm">
                                    <option value="5">5</option>
                                    <option value="10" selected="selected">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                </select>
                                entries
                            </label>

                           
                            <table class="table toggle-arrow-tiny table-striped dataTable" id="examplePagination" data-page-size="20">
                                <thead>
                                    <tr>
                                        <th>ชื่อผู้ยืม</th>
                                        <th>วัตถุประสงค์</th>
                                        <th>วันที่ยืม</th>
                                        <th>วันครบกำหนด</th>
                                        <th>จำนวนเงิน</th>
                                        <th>เลขที่ใบยืม</th>
                                    </tr>
                                </thead>
                               <tbody>
                            <asp:Repeater ID="rptList"  runat="server" OnItemDataBound="rptList_ItemDataBound">
										<ItemTemplate>        
                            <tr>
                                <td><asp:Label ID="lblName" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblObject" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblDateLoan" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblDateBack" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblAmount" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblNo" runat="server"></asp:Label></td>
                            </tr>
                            </ItemTemplate>
                           </asp:Repeater>
                        </tbody>
                                <tfoot>
                                       <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>รวม</td>
                                            <td><asp:Label ID="lblResult" runat="server"></asp:Label></td>
                                            <td></td>
                                       </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div>
                                                <ul class="pagination"></ul>
                                            </div>
                                        </td>
                                        <td></td>
                                    </tr>
                                    
                                </tfoot>
                            </table>
                    </div>
                </div>
         
   <div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"> <button data-toggle="dropdown" class="btn dropdown-toggle btn-primary" id="Button1">พิมพ์รายงาน <i class="icon-angle-down"></i></button>          
                        <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a href="Print_RecipientLoan.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print_RecipientLoan.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>           
                   </ul></div>
    </div>

       

    </div>

        </div>
      
        </div>
    </div> 
</div>   
  <!-- End Page -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScript" runat="server">
</asp:Content>
